'use strict';
var sequelize = require('../db');
var Sequelize = require('sequelize');

const ProgrammPoint = sequelize.define('programmpoints', {
    id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    eid: Sequelize.INTEGER,
    name: Sequelize.STRING,
    description: Sequelize.STRING,
    start: Sequelize.DATE,
    end: Sequelize.STRING,
    icon: Sequelize.STRING
});

ProgrammPoint.sync({ alter: true }).then(() => {

    console.log('create table programmpoint')
});


module.exports = ProgrammPoint;
