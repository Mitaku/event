'use strict';
var sequelize = require('../db');
var Sequelize = require('sequelize');

const Html = sequelize.define('html', {
    id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    eid: Sequelize.INTEGER,
    html: Sequelize.STRING
});

Html.sync({ alter: true }).then(() => {
    // Now the `users` table in the database corresponds to the model definition
    console.log('create table html')
});


module.exports = Html;
