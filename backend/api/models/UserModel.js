'use strict';
var sequelize = require('../db');
var Sequelize = require('sequelize');


const User= sequelize.define('users', {
    id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    firstName: Sequelize.STRING,
    lastName: Sequelize.STRING,
    email: Sequelize.STRING,
    username: {
        type: Sequelize.STRING,
        required: true
    },
    password: {
       type: Sequelize.STRING,
        required: true
    },
    role: Sequelize.STRING
});

User.sync({ alter: true }).then(() => {
    // Now the `users` table in the database corresponds to the model definition
    console.log('create table users')
});


module.exports = User;
