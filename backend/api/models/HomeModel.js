'use strict';
var sequelize = require('../db');
var Sequelize = require('sequelize');


const Home= sequelize.define('home', {
    id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    html: Sequelize.TEXT,
    css: Sequelize.TEXT
});

Home.sync({ alter: true }).then(() => {
    // Now the `users` table in the database corresponds to the model definition
    console.log('create table home')
});


module.exports = Home;
