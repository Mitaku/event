'use strict';
var sequelize = require('../db');
var Sequelize = require('sequelize');

const Event = sequelize.define('events', {
    id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    name: Sequelize.STRING,
    description: Sequelize.STRING,
    date: Sequelize.DATE,
    image: Sequelize.STRING
});

Event.sync({ alter: true }).then(() => {
    // Now the `users` table in the database corresponds to the model definition
    /**
    return User.create({
        firstName: 'John',
        lastName: 'Hancock'
    });**/
    console.log('create table events')
});


module.exports = Event;
