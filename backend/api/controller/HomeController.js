'use strict';
var sequelize = require('../db');
var Home = require('../models/HomeModel');


exports.get_home = function(req, res){
    Home.findByPk(req.params.id).then(home => {
        res.json(home);
    });
};

exports.add_home = function(req, res){

    const new_home = build_home(req);

    //if id exists ==> update
    Home.findByPk(req.params.id).then(home => {

        if(home){
            console.log('update');
            Home.update(
                {
                    html: req.body.html,
                    css: req.body.css
                },
                {where: {id: req.params.id}}).then((updated) => {
                res.json(updated);
            });

        } else { //new
            console.log('create');
            new_home.save().then((err,event) => {
                if(err)
                    res.send(err);
                res.json(event);
            });
        }
    });




};

function build_home(req){

return Home.build({
    html: req.body.html,
    css: req.body.css
});
};
