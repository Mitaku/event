'use strict';
var Sequelize = require('sequelize');
const Op = Sequelize.Op;
var sequelize = require('../db');
var User = require('../models/UserModel');
const  jwt  =  require('jsonwebtoken');
const  bcrypt  =  require('bcryptjs');

const secret  = require('../config').app.secret;
const EXPIRE_TIME = 24 * 60 * 60;

exports.register = function(req, res){

    const new_user = build_user(req);
    console.log(req.body);
    const  password  =  bcrypt.hashSync(req.body.password);
    new_user.password = password;

    new_user.save().then((user) => {

        console.log(user);
        const  accessToken  =  jwt.sign({ id:  user.id }, secret, {
            expiresIn:  EXPIRE_TIME
        });
        res.status(200).send({ "user":  user, "access_token":  accessToken, "expires_in":  EXPIRE_TIME});
    }, err => {
        res.send(err)
        });
};

exports.role = function(req, res){
     User.findOne(
        {where:  {id: req.body.userId}
            }).then(user => {
                console.log(user);
                if(user) {
                    res.status(200).send({ "role":  user.role});
                }
                else {
                  res.status(404).send({"err":"user not found"});
                }


        }, err  => {
        console.log("err");
            res.send(err);
        })

};



exports.login = function(req, res) {
console.log(req.body);
    User.findOne(
        {where:
                {[Op.or]:[
                       {username: {[Op.eq]: req.body.username}},
                       {email: {[Op.eq]: req.body.username}}
                    ]
                }
        }).then(user => {
            console.log(user);
            if(user) {
                const  result  =  bcrypt.compareSync(req.body.password, user.password);
                if(result){
                    const  expiresIn  =  24  *  60  *  60;
                                            const  accessToken  =  jwt.sign({ id:  user.id }, secret, {
                                                expiresIn:  EXPIRE_TIME
                                            });
                                            res.status(200).send({ "user":  user, "access_token":  accessToken, "expires_in":  EXPIRE_TIME});
                }
                else {
                    res.status(401).send({"err":"unauthorized"})
                }

            }
            else {
              res.status(401).send({"err":"unauthorized"});
            }


    }, err  => {
    console.log("err");
        res.send(err);
    })
};



function build_user(req){
    return User.build({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    });
};