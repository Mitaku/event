'use strict';
var sequelize = require('../db');
var Event = require('../models/EventModel');
var ProgrammPoint = require('../models/ProgrammPointModel')
var Html = require('../models/HtmlModel');

exports.list_all_events = function(req, res){
    Event.findAll().then(events => {
        res.json(events);
    });
};

exports.create_event = function(req, res){
    const new_event = Event.build({
        name: req.body.name,
        description: req.body.description,
        date: req.body.date,
        image: req.body.image
    });
    new_event.save().then(event => {

        const new_html = Html.build(
            {
                eid: event.id,
                html: req.body.html
            }
        );
        new_html.save().then(html => {

            const programmPoints = req.body.programm;



            for(let point of programmPoints){
                point.eid = event.id;
            }
            ProgrammPoint.bulkCreate(programmPoints).then((err, points) => {
                if(err)
                    res.send(err);
                res.json(event);
            }) ;

        }).catch(err => {
            res.send(err);
        });


      // res.json(event);
    }).catch(err => {
        res.send(err);
    });




};

exports.get_event = function(req, res) {
    Event.findByPk(req.params.id).then(event => {
        res.json(event);
    })
};


exports.get_programm = function(req, res){
    ProgrammPoint.findAll( {where: {eid : req.params.id}}).then(prgs => {
        res.json(prgs);
    })
}

//TODO update html and pp
exports.update_event = function(req, res) {
    Event.update(
        {
            name: req.body.name,
            description: req.body.description,
            date: req.body.date,
            image: req.body.image
        },
        {where: {id: req.params.id}}).then((updated) => {
        res.json(updated);
    })
};

exports.delete_event = function(req, res) {
    Event.destroy({
        where: {id : req.params.id}
    }).then((event) => {
        res.json(event);
    })
}


exports.get_html = function(req,res){
    Html.findOne(
        {where: {eid : req.params.id}}
        ).then(html => {
        res.json(html);
    })
}
