'use strict';
var sequelize = require('../db');
var User = require('../models/UserModel');


exports.list_all_user = function(req, res){
    User.findAll().then(users => {
        res.json(users);
    });
};

exports.create_user = function(req, res){

    const new_user = build_user(req);

    new_user.save().then((err,event) => {
        if(err)
            res.send(err);
        res.json(event);
    });
};

exports.get_user = function(req, res) {
    User.findByPk(req.params.id).then(event => {
        res.json(event);
    })
};

exports.update_user = function(req, res) {
    User.update(
        {
            firstName: req.body.fristName,
            lastName: req.body.lastName,
            email: req.body.email,
            username: req.body.username,
            password: req.body.password,
            role: req.body.role
        },
        {where: {id: req.params.id}}).then((updated) => {
        res.json(updated);
    })
};

exports.delete_user = function(req, res) {
    User.destroy({
        where: {id : req.params.id}
    }).then((user) => {
        res.json(user);
    })
};

function build_user(req){
    return User.build({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        role: req.body.role
    });
};