const db_config = require('./config').db;


var Sequelize = require('sequelize');

const sequelize = new Sequelize(
    db_config.database, db_config.username, db_config.password, {
        host: db_config.host,
        port: db_config.port,
        dialect: db_config.dialect
    }
);
//const sequelize =  new Sequelize('postgres://postgres:root@localhost:5432/postgres');

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });


module.exports = sequelize;
