'use strict';
const  jwt  =  require('jsonwebtoken');
const secret  = require('../config').app.secret;

module.exports = function(app){
    var auth = require('../controller/AuthController');


    app.route('/login')
        .post(auth.login);

    app.route('/register')
        .post(auth.register);

    app.use('/role', validateUser, auth.role);

/*
    app.route('/role')
        .post(auth.role);
*/


    function validateUser(req, res, next){
        console.log(req.headers);
        jwt.verify(req.headers['x-access-token'], secret, (err, decoded) => {
            if(err){
                res.json({status: 'error', message: err.message, data: null});
            } else {
                req.body.userId = decoded.id;       //ass userId to request
                next();
            }
        })
    }

};
