'use strict';
const  jwt  =  require('jsonwebtoken');
const secret  = require('../config').app.secret;

module.exports = function(app){
    var event = require('../controller/EventController');


    app.route('/events')
        .get(event.list_all_events);
     //   .post(event.create_event);

    app.route('/event/:id')
        .get(event.get_event)
        .put(event.update_event);
   //     .delete(event.delete_event);
    app.route('/event/:id/program')
        .get(event.get_programm);

    app.route('/event/:id/html')
        .get(event.get_html);


    app.post('/events', validateUser, event.create_event);

    app.delete('/event/:id', validateUser, event.delete_event);

    /*
        app.route('/role')
            .post(auth.role);
    */


    function validateUser(req, res, next){
        console.log(req.headers);
        jwt.verify(req.headers['x-access-token'], secret, (err, decoded) => {
            if(err){
                res.json({status: 'error', message: err.message, data: null});
            } else {
                req.body.userId = decoded.id;       //ass userId to request
                next();
            }
        })
    }

};
