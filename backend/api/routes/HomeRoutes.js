'use strict';
module.exports = function(app){
    var home = require('../controller/HomeController');


    app.route('/home/:id')
        .get(home.get_home)
        .post(home.add_home);



};
