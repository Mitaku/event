'use strict';
module.exports = function(app){
    var user = require('../controller/UserController');


    app.route('/users')
        .get(user.list_all_user)
         .post(user.create_user);

    app.route('/user/:id')
        .get(user.get_user)
        .put(user.update_user)
        .delete(user.delete_user);


};
