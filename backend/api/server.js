const PORT = require('./config').app.port;
const bodyParser = require('body-parser');
const cors = require('cors');
var express = require('express'),
    app = express(),
    port = process.env.PORT || PORT;

app.use(cors());


app.use(express.json());


var routes = require('./routes/EventRoutes');
var user_routes = require('./routes/UserRoutes');
var auth = require('./routes/AuthRoutes');
var home = require('./routes/HomeRoutes');

routes(app);
user_routes(app);
auth(app);
home(app);



app.listen(port);
console.log('Server startet on: '+port);
