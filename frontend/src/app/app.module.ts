import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/Storage';
import {FormBuilder, ReactiveFormsModule} from '@angular/forms';
import {HttpConfigInterceptor} from './services/httpConfig.interceptor';
import {AddPage} from './pages/list/add/add.page';
import {AddProgrammPage} from './pages/events/programm/add-programm/add-programm.page';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { NgInitDirective } from './directives/ng-init.directive';

@NgModule({
  declarations: [
      AppComponent,
      AddPage,
      AddProgrammPage
  ],
  entryComponents: [
      AddPage,
      AddProgrammPage
  ],
  imports: [
      BrowserModule,
      IonicModule.forRoot(),
      AppRoutingModule,
      HttpClientModule,
      IonicStorageModule.forRoot(),
      ReactiveFormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
      {provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
