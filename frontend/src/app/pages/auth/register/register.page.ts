import { Component, OnInit } from '@angular/core';
import {AuthentificationService} from '../../../services/authentification.service';
import {Router} from '@angular/router';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;

  constructor(private router: Router, private auth: AuthentificationService, public formBuilder: FormBuilder) {
    this.registerForm = formBuilder.group({
      username: [''],
      firstName: ['', Validators.pattern('^[a-zA-Z]+')],
      lastName: ['', Validators.pattern('^[a-zA-Z]+')],
      email: ['', Validators.email],
      password: ['', Validators.compose([Validators.minLength(8)])],
      confirm: ['', [this.equalto('password')]]
    });
  }


  equalto(fieldName): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const input = control.value;
      const isValid = control.root.value[fieldName] === input;
      if (!isValid) {
        return {'equalTo': {isValid}};
      } else {
        return null;
      }
    };
  }



  ngOnInit() {
  }

  getForm() {
    return this.registerForm.controls;
  }

  register(form) {
    if (this.registerForm.invalid) {
      return;
    }

    const user = {
    firstName: this.registerForm.value.firstName,
    lastName: this.registerForm.value.lastName,
    username: this.registerForm.value.username,
    email: this.registerForm.value.email,
    password: this.registerForm.value.password,
      role: 'USER'
  };
    console.log(user);
    if (this.registerForm.value.password === this.registerForm.value.confirm) {
      //TODO check pw length, if username / mail exists
      this.auth.register(user).subscribe(data => {
        this.router.navigateByUrl('/menu/login');
      });
    } else {
      //TODO
    }
  }
}
