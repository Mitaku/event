import { Component, OnInit } from '@angular/core';
import {JSONService} from '../../services/json.service';
import {Event, GlobalService, ProgrammPoint} from '../../services/global.service';
import 'grapesjs/dist/css/grapes.min.css';
import grapesjs from 'grapesjs';
import 'node_modules/grapesjs-preset-webpage';
import {AuthentificationService} from '../../services/authentification.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

    private saveSteps = 0;
    private HomeId = 1;

  constructor(public auth: AuthentificationService, public json: JSONService) {
  }

  ngOnInit() {
//TODO one page change
      this.json.getHome(this.HomeId).subscribe(data => {
          console.log(data);
          if (data) {
              console.log("return:");
              console.log(data);
         //     document.getElementById('gjs').innerHTML = data.html;
          }
          if (this.auth.isAdmin()) {
              this.loadEditor();
          }

      });

  }


  save(html, css) {
      this.saveSteps++;
      console.log("saved");
      if (this.saveSteps === 5) {

          html = {'html': html, 'css': css};
          console.log("update");
          this.json.updateHome(this.HomeId, html).subscribe(data => {
          });
          this.saveSteps = 0;
      }
  }

  loadEditor() {
      var editor = grapesjs.init({
          height: '100%',
          showOffsets: 1,
          noticeOnUnload: 0,
          storageManager: {
            type: 'remote',
              stepsBeforeSave: 3,
              urlStore: 'http://localhost:3000/home/1',
              urlLoad: 'http://localhost:3000/home/1',
              headers: {'x-access-token': this.auth.getToken()}
          },
          container: '#gjs',
          fromElement: true,
          plugins: ['gjs-preset-webpage'],
          pluginsOpts: {
              'gjs-preset-webpage': {}
          }
      });

      editor.on('update', () => {
          this.save(editor.getHtml(), editor.getCssValue());
      });

  }

}
