import { Component, OnInit } from '@angular/core';
import {JSONService} from '../../services/json.service';
import {GlobalService} from '../../services/global.service';
import {AuthentificationService} from '../../services/authentification.service';
import {ModalController, NavController} from '@ionic/angular';
import {AddPage} from './add/add.page';
import {Router} from '@angular/router';
import {IEvent} from '../../services/user';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {


  public event_list = [];

  constructor(private router: Router, private nav: NavController, private jsonService: JSONService, private global: GlobalService, private auth: AuthentificationService, private modal: ModalController) {

  }

  ngOnInit() {
    this.jsonService.getLocalData().subscribe(data => {

      this.event_list = []; // reset list!!

      //TODO delete after development
      for (const event of data) {
        this.event_list.push(this.global.jsonToEvent(event));
      }
      this.global.events = this.event_list;
    });



  }

  ionViewWillEnter() {
    this.jsonService.getAllEvents().subscribe(data => {
      console.log(data);
      for (const event of data) {
        this.event_list.push(event);
      }
    });
  }


  public openEvent(id) {
    this.router.navigateByUrl('events/' + id);
  }

  public isAdmin(): boolean {
    return this.auth.isAdmin();
  }

  async openAddModal() {
    const addModal = await this.modal.create({
      component: AddPage
    });

    addModal.onDidDismiss().then(data => {
      console.log(data);
      this.global.createdEvent = data.data;
      // this.event_list.push(data.data);
      this.router.navigateByUrl('events/create');

    });

    await addModal.present();
  }


  public edit(event) {
    //open in edit mode
    const route = 'events/' + event.id;
    this.router.navigate([route], {
      queryParams: {
        edit: true
      }
    });
  }

  public delete(event) {
    //TODO question
    this.jsonService.deleteEvent(event.id).subscribe(data => {
      console.log(data);
      //TODO play toast
      this.event_list = this.event_list.filter(obj => obj !== event);
    });
  }

}
