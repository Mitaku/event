import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DatePicker} from '@ionic-native/date-picker';
import {JSONService} from '../../../services/json.service';
import {IEvent} from '../../../services/user';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  private eventForm: FormGroup;

  constructor(private modal: ModalController, private formBuilder: FormBuilder, private server: JSONService) {
    this.eventForm = formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      date: ['', Validators.required],
      image: ['']
    });
  }

  ngOnInit() {
  }

  private close() {
    this.modal.dismiss();
  }

  private create() {
    //TODO
    const event: IEvent = {
      name: this.eventForm.value.name,
      description: this.eventForm.value.description,
      date: this.eventForm.value.date,
      image: this.eventForm.value.image
    };

    /*
    this.server.addEvent(event).subscribe(data => {
      if (data.id) {
        console.log(data.id);
      } else {
        //error taost
      }*/
    this.modal.dismiss(event);

  }


  private today() {
    return new Date().toISOString();
  }

  private getMaxTime() {
    const date = new Date();
    date.setFullYear(new Date().getFullYear() + 5);
    return date.toISOString();
  }

}
