import { Component, OnInit } from '@angular/core';
import {AuthentificationService} from '../../services/authentification.service';
import {Router} from '@angular/router';
import {ToastController} from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private router: Router, private auth: AuthentificationService, private toastController: ToastController) {
  }

  ngOnInit() {
  }


  login(form) {
    this.auth.login(form.value.name, form.value.password).subscribe(data => {
      this.router.navigateByUrl('/home');
    }, async err => {
        const toast = await this.toastController.create({
          message: 'Benutzername oder Password falsch',
          duration: 2000
        });
        toast.present();
    });
  }
}


