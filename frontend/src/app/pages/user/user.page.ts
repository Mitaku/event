import { Component, OnInit } from '@angular/core';
import {AuthentificationService} from '../../services/authentification.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {

  constructor(private router: Router, private auth: AuthentificationService) {
  }

  ngOnInit() {
  }


  logout() {
    this.auth.logout();
    this.router.navigateByUrl('/login');

  }
}
