import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProgrammPage } from './add-programm.page';

describe('AddProgrammPage', () => {
  let component: AddProgrammPage;
  let fixture: ComponentFixture<AddProgrammPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddProgrammPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProgrammPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
