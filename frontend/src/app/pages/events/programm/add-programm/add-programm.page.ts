import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalController} from '@ionic/angular';
import {JSONService} from '../../../../services/json.service';

@Component({
  selector: 'app-add-programm',
  templateUrl: './add-programm.page.html',
  styleUrls: ['./add-programm.page.scss'],
})
export class AddProgrammPage implements OnInit {

  private ProgrammForm: FormGroup;

  constructor(private modal: ModalController, private formBuilder: FormBuilder) {
    this.ProgrammForm = formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      start: ['', Validators.required],
      end: ['', Validators.required],
      icon: ['']
    });
  }

  ngOnInit() {
  }


  close() {
    this.modal.dismiss();
  }

  create() {
    console.log(this.ProgrammForm.value);
    this.modal.dismiss(this.ProgrammForm.value);
  }

}
