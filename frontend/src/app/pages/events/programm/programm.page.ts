import { Component, OnInit } from '@angular/core';
import {GlobalService} from '../../../services/global.service';
import {ActivatedRoute} from '@angular/router';
import {AddPage} from '../../list/add/add.page';
import {ModalController} from '@ionic/angular';
import {AddProgrammPage} from './add-programm/add-programm.page';
import {BehaviorSubject} from 'rxjs';
import {IEvent, IProgrammPoint} from '../../../services/user';
import {JSONService} from '../../../services/json.service';

@Component({
  selector: 'app-programm',
  templateUrl: './programm.page.html',
  styleUrls: ['./programm.page.scss', 'timeline.css'],
})
export class ProgrammPage {

  //private items = [];
  public items: IProgrammPoint[] = [];
  private creatorMode = false;

  constructor(private global: GlobalService, private route: ActivatedRoute, private modal: ModalController, private server: JSONService) {}

  ionViewWillEnter() {

    let id;
    let route = this.route.snapshot.root;
    do {
      if (route.params['id']) {
        id = route.params['id'];
      }
      route = route.firstChild;
    }while (route);

    if (id === 'create') {
      this.creatorMode = true;
    } else {
      console.log('load Data');
      this.server.getProgramm(id).subscribe(data => {
        this.items = data;
      });
    }

    //TODO load programm!!!


  /*
    if (this.global.selectedEvent) {
      this.items.next([]);
      const programm = this.global.selectedEvent.programm;
      for (const el of programm) {
        console.log(el);
        this.items.next(this.items.value.push(el));
      }
    }
    */

  }


  async create() {
    const addModal = await this.modal.create({
      component: AddProgrammPage
    });

    addModal.onDidDismiss().then(data => {
      if (data.data) {
        this.items.push(data.data);
        this.global.createdEvent.programm = this.items;
      }
    });
    await addModal.present();
  }


  private delete(item) {
    this.items = this.items.filter(obj => obj !== item);
    this.global.createdEvent.programm = this.items;
  }

  private doReorder(event) {
    //TODO reorder list
    event.detail.complete();
    this.global.createdEvent.programm = this.items;
  }

}
