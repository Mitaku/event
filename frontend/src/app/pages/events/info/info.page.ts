import {Component, OnInit, Pipe, PipeTransform, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {QuillEditorComponent} from 'ngx-quill';
import {GlobalService} from '../../../services/global.service';
import {JSONService} from '../../../services/json.service';
import {DomSanitizer} from '@angular/platform-browser';
import {AuthentificationService} from '../../../services/authentification.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  private newEvent = false;
  private creatorMode = false;
  private editor;
  private editorInstance;
  private content;

  constructor(private route: ActivatedRoute, private global: GlobalService, private server: JSONService) { }

  ngOnInit() {
    let id;
    let route = this.route.snapshot.root;
    do {
      if (route.params['id']) {
        id = route.params['id'];
      }
      route = route.firstChild;
    }while (route);

    if (id === 'create') {
      this.creatorMode = true;
      this.newEvent = true;
    }

    this.server.getHtml(id).subscribe(data => {
      this.content = data.html;
      console.log(data.html);

      const range = this.editorInstance.getSelection(true);
      this.editorInstance.clipboard.dangerouslyPasteHTML(this.content);
    });


    if (this.route.snapshot.queryParams.edit) {
      this.creatorMode = true;
    }


  }

  created(editor) {
    this.editorInstance = editor;
  }

  changed() {
    if (this.newEvent) {
      this.global.createdEvent.html = this.editor;
    }
  }

}