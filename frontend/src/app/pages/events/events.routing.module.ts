import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventsPage } from './events.page';

const routes: Routes = [
    {
        path: ':id',
        component: EventsPage,
        children: [
            {
                path: 'info',
                children: [
                    {
                        path: '',
                        loadChildren: './info/info.module#InfoPageModule'
                    }
                ]
            },
            {
                path: 'program',
                children: [
                    {
                        path: '',
                        loadChildren: './programm/programm.module#ProgrammPageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: 'info',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: 'info',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class EventsPageRoutingModule {}
