import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {JSONService} from '../../services/json.service';
import {IEvent} from '../../services/user';
import {GlobalService} from '../../services/global.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {

  private creatorMode = false;

  constructor(private router: Router, private route: ActivatedRoute, private server: JSONService, private global: GlobalService) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    console.log(id);

    if (id === 'create') {
      this.creatorMode = true;
    }
  }

  save() {
    console.log('save');
    console.log(this.global.createdEvent);
    this.server.addEvent(this.global.createdEvent).subscribe(data => {
      //TODO
      if (data) {
        this.router.navigateByUrl('/list'); // TODO ??
      }
    });
  }


}
