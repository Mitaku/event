import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {AuthentificationService} from '../services/authentification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard {

  constructor(public auth: AuthentificationService) {}

  canActivate(): boolean {
    return this.auth.isAuthentificated();
  }
}
