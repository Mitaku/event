import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';
import {AuthentificationService} from './services/authentification.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  pages = [
    {
      id: 'home',
      title: 'Home',
      icon: 'home',
      url: '/home'
    },
    {
      id: 'cal',
      title: 'Events',
      icon: 'calendar',
      url: '/list'
    },
    {
      id: 'about',
      title: 'About',
      icon: 'contact',
      url: '/about'
    }
  ];

  loginPageModel = {
    id: 'user',
    title: 'Login',
    icon: 'log-in',
    url: '/login'
  };

  userPageModel = {
    id: 'user',
    title: 'User',
    icon: 'person',
    url: '/user'
  };

  loginPage = this.loginPageModel;


  private selectedPath = '';

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private auth: AuthentificationService
  ) {
    this.initializeApp();

    this.router.events.subscribe((event: RouterEvent) => {
      if (event instanceof NavigationEnd) {
        this.selectedPath = event.url;
      }

      if (auth.authentificationState) {
        this.loginPage = this.userPageModel;
      } else {
        this.loginPage = this.loginPageModel;
      }

      auth.authentificationState.subscribe(state => {

        if (state) {
          this.loginPage = this.userPageModel;
        } else {
          this.loginPage = this.loginPageModel;
        }
      });
    });
  }

  isActive(url): boolean {
    return (this.selectedPath.startsWith(url));
  }


  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
