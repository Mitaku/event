//httpConfig.interceptor.ts
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import {from, Observable, throwError} from 'rxjs';
import {map, catchError, switchMap} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import {AuthentificationService} from './authentification.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

    constructor(private auth: AuthentificationService) { }


     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


         return from(this.auth.getToken()).pipe(switchMap(token => {
             if (token) {
                 request = request.clone({
                     setHeaders: {
                         'x-access-token': token
                     }
                 });
             }


             if (!request.headers.has('Content-Type')) {
                 request = request.clone({
                     setHeaders: {
                         'content-type': 'application/json'
                     }
                 });
             }

             request = request.clone({
                 headers: request.headers.set('Accept', 'application/json')
             });

             return next.handle(request).pipe(
                 map((event: HttpEvent<any>) => {
                     if (event instanceof HttpResponse) {
                         console.log('event--->>>', event);
                     }
                     return event;
                 }),
                 catchError((error: HttpErrorResponse) => {
                     console.log('wwwerror---->>>>');
                     console.error(error);
                     return throwError(error);
                 }));

         }));

     }

}
