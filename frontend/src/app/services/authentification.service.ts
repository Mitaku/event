import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Platform, ToastController} from '@ionic/angular';
import {Storage} from '@ionic/Storage';
import {HttpClient} from '@angular/common/http';
import {AuthResponse, User} from './user';
import {tap} from 'rxjs/operators';

const TOKEN_KEY = 'auth-token';
const EXPIRE_KEY = 'expires-in';
const AUTH_SERVER_HOST = 'http://localhost';
const AUTH_PORT = '3000';
@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  authentificationState = new BehaviorSubject(false);
  role = undefined;

  constructor(private storage: Storage, private plt: Platform, private http: HttpClient) {
   this.storage.get(TOKEN_KEY).then(data => {
     if (data) {
         if (!this.role) {
            this.http.get(`${AUTH_SERVER_HOST}:${AUTH_PORT}/role`).subscribe( res => {
                 if (res['role']) {
                     this.role = res['role'];
                 } else {
                     // TODO err
                 }
             });
         }
         this.authentificationState.next(true);
     }
   });

  }


  login(name: string, pw: string): Observable<any> {
    const user = {
    username : name,
    password : pw
    };

    return this.http.post(`${AUTH_SERVER_HOST}:${AUTH_PORT}/login`, user).pipe(
        tap(async (res: AuthResponse) => {
          if (res.user) {
            await this.storage.set(TOKEN_KEY, res.access_token);
            await this.storage.set(EXPIRE_KEY, res.expires_in);
            this.role = res.user.role;
            this.authentificationState.next(true);
            console.log('test');
          }
        })
    );
  }

  async logout() {
    await this.storage.remove(TOKEN_KEY);
    await this.storage.remove(EXPIRE_KEY);
    this.authentificationState.next(false);
  }

  isAuthentificated() {
    return this.authentificationState.value;
  }

   public getToken(): Promise<any> {
      return this.storage.get(TOKEN_KEY);
  }

  isAdmin() {
     if (this.role === 'ADMIN') {
         return true;
     }
     return false;
  }

  register(user): Observable<any> {
    return this.http.post(`${AUTH_SERVER_HOST}:${AUTH_PORT}/register`, user).pipe(
        tap(async (res: AuthResponse) => {
          if (res.access_token) {
            await this.storage.set(TOKEN_KEY, res.access_token);
            await this.storage.set(EXPIRE_KEY, res.expires_in);
            this.role = res.user.role;
            this.authentificationState.next(true);
          }
        })
    );
  }

}
