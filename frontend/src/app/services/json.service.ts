import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {AuthResponse, IEvent, IProgrammPoint} from './user';

const RES_SERVER_HOST = 'http://localhost';
const RES_PORT = '3000';

@Injectable({
  providedIn: 'root'
})
export class JSONService {

  constructor(private http: HttpClient) { }

  getLocalData(): Observable<any> {
    return this.http.get('./assets/data.json').pipe(map(results => results));
  }

  getEvent(id): Observable<any> {
    return this.http.get('./assets/data-id.json').pipe(map(results => results));
  }


  getAllEvents(): Observable<any> {
    return this.http.get(`${RES_SERVER_HOST}:${RES_PORT}/events`).pipe(map(results => results));
  }

  addEvent(event: IEvent): Observable<any> {
    return this.http.post(`${RES_SERVER_HOST}:${RES_PORT}/events`, event);
  }

  getProgramm(eventID: number): Observable<any> {
    return this.http.get(`${RES_SERVER_HOST}:${RES_PORT}/event/${eventID}/program`).pipe(map(results => results));
  }

  getHtml(eventID: number): Observable<any> {
    return this.http.get(`${RES_SERVER_HOST}:${RES_PORT}/event/${eventID}/html`).pipe(map(results => results));
  }

  deleteEvent(eventID): Observable<any> {
    return this.http.delete(`${RES_SERVER_HOST}:${RES_PORT}/event/${eventID}`);
  }


  getHome(id): Observable<any> {
    return this.http.get(`${RES_SERVER_HOST}:${RES_PORT}/home/${id}`);
  }

  updateHome(id, html): Observable<any> {
    return this.http.post(`${RES_SERVER_HOST}:${RES_PORT}/home/${id}`, html);
  }


}
