import { Injectable } from '@angular/core';
import {Time} from '@angular/common';
import {IEvent} from './user';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor() { }

  public events: Event[] = [];

  public selectedEvent: Event = null;

  //holds data until event is saved
  public createdEvent: IEvent;


  public jsonToEvent(event): Event{
      const eventObj = new Event();
      eventObj.name = event["name"];
      eventObj.date = event['date'];
      eventObj.description = event['description'];
      eventObj.programm = [];
      eventObj.id = event['id'];

      for (const point of event['programm']) {
        const programmObj = new ProgrammPoint();
        programmObj.description = point['description'];
        programmObj.name = point['name'];
        programmObj.timeStart = point['timeStart'];
        programmObj.timeEnd = point['timeEnd'];
        programmObj.icon = point['icon'];
        eventObj.programm.push(programmObj);
      }

      return eventObj;
  }
}

export class Event {
  public name: string;
  public description: string;
  public date: Date;
  public id?: number;
  public programm: ProgrammPoint[];
}

export class ProgrammPoint {
  public name: string;
  public description: string;
  public timeStart: Time;
  public timeEnd: Time;
  public icon: string;
}