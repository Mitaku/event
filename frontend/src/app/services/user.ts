export interface User {
    firstName: string;
    lastName: string;
    username: string;
    email: string;
    password: string;
    role: string;
}

export interface AuthResponse {
    user: {
        id: number,
        name: string,
        email: string,
        role: string
    };
    access_token: string;
    expires_in: number;
    role: string;
}

export interface IEvent {
    id?: number;
    name: string;
    description: string;
    date: string;
    image: string;
    html?: string;
    programm?: IProgrammPoint[];
}

export interface IProgrammPoint {
    name: string;
    description: string;
    start: Date;
    end: Date;
    icon: string;
}