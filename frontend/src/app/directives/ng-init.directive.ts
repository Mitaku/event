import {Directive, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Directive({
  selector: '[appNgInit]'
})
export class NgInitDirective implements OnInit {

  @Output('appNgInit') initEvent: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.initEvent.emit();
  }
}
